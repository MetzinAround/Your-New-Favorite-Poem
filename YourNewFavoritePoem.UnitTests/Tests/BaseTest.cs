﻿using System;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using NUnit.Framework;
using Your_New_Favorite_Poem;
using Your_New_Favorite_Poem.Constants;
using Your_New_Favorite_Poem.Database;

namespace YourNewFavoritePoem.UnitTests.Tests
{
    class BaseTest
    {
        AuthorsDbContext? authorsDbContext;

        protected AuthorsDbContext AuthorsDbContext => authorsDbContext ?? throw new NullReferenceException();

        [SetUp]
        public void Setup()
        {
            var optionsBuilder = new DbContextOptionsBuilder<AuthorsDbContext>().UseInMemoryDatabase(nameof(YourNewFavoritePoem));

            authorsDbContext = new AuthorsDbContext(optionsBuilder.Options);

            DbInitializer.Initialize(authorsDbContext);
        }

        [TearDown]
        public void TearDown()
        {
            foreach(var author in AuthorsDbContext.Authors)
            {
                var doesExist = PoemsConstants.AuthorList.Any( x => x.Name == author.Name);
                if (!doesExist)
                {
                    AuthorsDbContext.Authors.Remove(author);
                    AuthorsDbContext.SaveChanges();
                }
            }

            foreach (var poems in AuthorsDbContext.Poems)
            {
                var doesExist = PoemsConstants.AuthorList.SelectMany(x => x.Poems).Any(x => x.Title == poems.Title); ;
                if (!doesExist)
                {
                    AuthorsDbContext.Poems.Remove(poems);
                    AuthorsDbContext.SaveChanges();
                }
            }
        }
    }
}
